#!/*bin/bash
#   This script must be executed by the superuser
echo Start build of ubuntu enviroment by @jorgescalona ........

#   copy ssh keys to project folder
cp -vrf ~/.ssh .

#  pool by enviromet vars and create env-file
export GITEMAIL=`git config user.email` && echo GITEMAIL='"'$GITEMAIL'"' >> env.list
export GITUSER=`git config user.name` && echo GITUSER='"'$GITUSER'"' >> env.list
export hoy=`date "+%m_%d_%y_%H_%M_%S"`
export ABS_PATH=`pwd` && export LOCAL_BASENAME=`basename $ABS_PATH` && \
export LOCAL_IMAGE_NAME="$LOCAL_BASENAME/$hoy:v_1_2" && echo Build IT: $LOCAL_IMAGE_NAME ....!!!

#  Docker build image here if arg its 0 then no cache build
if [ "$1" = "0" ]; then
    docker build -f Dockerfile . -t  $LOCAL_IMAGE_NAME --no-cache
else
    docker build -f Dockerfile . -t  $LOCAL_IMAGE_NAME
fi

#  Run container
echo -e "PreBuild ist succesfully ! ........ \n  Run Container!!!  Enjoy It!"

#  docker run container
docker run --env-file ./env.list -itP -e LANG=C.UTF-8 --entrypoint="tmux" -itP $LOCAL_IMAGE_NAME
rm -vrf .ssh/ env.list
exit 0
